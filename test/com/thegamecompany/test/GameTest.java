package com.thegamecompany.test;

import com.thegamecompany.game.GameSetting;
import com.thegamecompany.game.PuzzleGame;

public class GameTest {

	public void testGameFixed() {
		System.out.println("GameFixed");
		GameSetting setting = new GameSetting(2, 2);
		int[][] cells = {{1, 3}, {0, 2}};
		PuzzleGame game = new PuzzleGame(setting, cells);
		game.dumpCells();
	}

	public void testGameRandom() {
		System.out.println("GameRandom");
		GameSetting setting = new GameSetting(3, 3);
		PuzzleGame game = new PuzzleGame(setting);
		game.dumpCells();
		System.out.println("GameRandom - reset");
		game.resetGame();
		game.dumpCells();
	}
	
	public void testGameDone() {
		System.out.println("GameDone");
		GameSetting setting = new GameSetting(2, 2);
		int[][] cells = {{1, 2}, {3, 0}};
		PuzzleGame game = new PuzzleGame(setting, cells);
		game.dumpCells();
		System.out.println("gameDone: " + game.isDone());
	}

	public void testGameNotDone() {
		System.out.println("GameNotDone");
		GameSetting setting = new GameSetting(2, 2);
		int[][] cells = {{1, 2}, {0, 3}};
		PuzzleGame game = new PuzzleGame(setting, cells);
		game.dumpCells();		
		System.out.println("gameNotDone: " + game.isDone());
		
		System.out.println("canMoveCell (0,0): " + game.canMoveCell(0, 0));
		System.out.println("canMoveCell (0,1): " + game.canMoveCell(0, 1));
		System.out.println("canMoveCell (0,1): " + game.canMoveCell(1, 1));

		game.moveCell(1, 1);
		System.out.println("gameNotDone: " + game.isDone());
	}

	public static void main(String[] args) {
		GameTest test = new GameTest();
		test.testGameFixed();
		test.testGameRandom();
		test.testGameDone();
		test.testGameNotDone();
	}

}