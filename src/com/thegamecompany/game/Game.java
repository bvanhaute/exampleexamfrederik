package com.thegamecompany.game;

public abstract class Game implements Resettable {
	
	protected GameSetting setting;

	public Game(GameSetting setting) {
		super();
		if (setting == null) {
			throw new IllegalArgumentException("a GameSetting should be provided as input parameter");
		}
		this.setting = setting;
	}

	public int getRows() {
		return setting.getRows();
	}

	public int getCols() {
		return setting.getCols();
	}
	
	public abstract boolean isDone();

	
}
