package com.thegamecompany.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PuzzleGame extends Game {

	// 2-dimensionele array met unieke waarden per cell
	// 0 duidt de "lege" cell aan
	int[][] cells;

	/*
	 * constructor use GameSetting to set rows & cols properties reset the game
	 * by calling resetGame() to randomize cells
	 */
	public PuzzleGame(GameSetting setting) {
		super(setting);
		cells = new int[setting.getCols()][setting.getRows()];
		resetGame();
	}

	/*
	 * constructor takes GameSetting and 2-dimensional array as cells
	 */
	public PuzzleGame(GameSetting setting, int[][] cells) {
		super(setting);
		checkInputIsValid(setting, cells);
		this.cells = makeDefensiveCopy(cells);
	}

	private int[][] makeDefensiveCopy(int[][] original) {
		final int[][] result = new int[original.length][];
		for (int i = 0; i < original.length; i++) {
			result[i] = Arrays.copyOf(original[i], original[i].length);
		}
		return result;
	}

	private void checkInputIsValid(GameSetting setting, int[][] inputCells) {
		int actualColumns = inputCells.length;
		int actualRows = (inputCells.length > 0 ? inputCells[0].length : -1);
		if (actualColumns != setting.getCols() || actualRows != setting.getRows()) {
			throw new IllegalArgumentException("the two dimensional array given as input should have "
					+ setting.getCols() + " columns and " + setting.getRows() + " rows, but had " + actualColumns
					+ " columns and " + actualRows + " rows");
		}
	}

	/*
	 * returns value for given cell
	 */
	public int getCell(int row, int col) {
		if (row < 0 || row >= setting.getRows()) {
			throw new IllegalArgumentException(
					"invalid rownumber " + row + ", must be between 0 and " + (setting.getRows() - 1));
		}
		if (col < 0 || col >= setting.getCols()) {
			throw new IllegalArgumentException(
					"invalid colnumber " + col + ", must be between 0 and " + (setting.getCols() - 1));
		}
		return cells[row][col];
	}

	/*
	 * use GameSetting to set rows & cols properties create new 2 dimensional
	 * array to hold values load valid random value (e.g. 4x4: 0->15, 0 value
	 * indicate "empty" cell)
	 */
	@Override
	public void resetGame() {
		List<Integer> values = new ArrayList<>();
		for (int i = 0; i < (setting.getRows() * setting.getCols()); i++) {
			values.add(i);
		}
		Collections.shuffle(values);
		Iterator<Integer> valueIterator = values.iterator();
		for (int i = 0; i < cells.length; i++) {
			int[] js = cells[i];
			for (int j = 0; j < js.length; j++) {
				js[j] = valueIterator.next();
			}
		}
	}

	/*
	 * checks if given cell can be moved (cell is adjacent to "empty" cell
	 */
	public boolean canMoveCell(int row, int col) {
		Position pos = getEmptyNeighbour(row, col);
		return pos != null;
	}

	private Position getEmptyNeighbour(int row, int col) {
		List<Position> positionsToCheck = new ArrayList<>();
		addPositionIfOnBoard(positionsToCheck, row - 1, col);
		addPositionIfOnBoard(positionsToCheck, row + 1, col);
		addPositionIfOnBoard(positionsToCheck, row, col - 1);
		addPositionIfOnBoard(positionsToCheck, row, col + 1);
		for (Position position : positionsToCheck) {
			if (cells[position.getRow()][position.getColumn()] == 0) {
				return position;
			}
		}
		return null;
	}

	private void addPositionIfOnBoard(List<Position> positionsToCheck, int row, int col) {
		if (row >= 0 && row < setting.getRows() && col >= 0 && col < setting.getCols()) {
			positionsToCheck.add(new Position(row, col));
		}
	}

	/*
	 * move a given cell to the empty spot swaps the value of the cells with the
	 * "empty" cell
	 */
	public void moveCell(int row, int col) {
		Position pos = getEmptyNeighbour(row, col);
		if (pos == null) {
			throw new IllegalArgumentException("moveCell called on position that cannot move");
		}
		swapTiles(new Position(row, col), pos);
	}

	private void swapTiles(Position source, Position target) {
		int temp = cells[target.getRow()][target.getColumn()];
		cells[target.getRow()][target.getColumn()] = cells[source.getRow()][source.getColumn()];
		cells[source.getRow()][source.getColumn()] = temp;
	}

	/*
	 * dump game status on console iterate across 2 dimensional array and
	 * display cells display as shown in the GUI
	 */
	public void dumpCells() {
		System.out.println("");
		System.out.println("gameBoard: ");
		for (int i = 0; i < cells.length; i++) {
			printColumn(cells[i]);
		}
		System.out.println("");
	}

	private void printColumn(int[] is) {
		System.out.println();
		for (int i = 0; i < is.length; i++) {
			System.out.print("\t" + is[i]);
		}
	}

	/*
	 * check if the game is done
	 */
	@Override
	public boolean isDone() {
		// go through game with expected value : each field should have value
		// one higher than previous one, starting with 1
		// the last field should be the empty one
		int expectedValue = 1;
		for (int i = 0; i < cells.length; i++) {
			int[] row = cells[i];
			for (int j = 0; j < row.length; j++) {
				if (i == cells.length - 1 && j == row.length -1) {
					// the last field, previous fields contained all the numbers
					// from 1 to max,
					if (row[j] == 0) {
						return true;
					} else {
						throw new IllegalArgumentException(
								"last field should have been the empty field, invalid game board");
					}
				}
				// check value
				if (row[j] != expectedValue) {
					return false;
				} else {
					expectedValue++;
				}
			}
		}
		throw new IllegalArgumentException("coding error : should already have returned the solution ");
	}

}
